exports.default = {
    'agenda' : {},
    'accordeon': {},
    'arguments' : {},
    'arguments-form' : {},
    'section' : {
      addLabel: 'Columns',
      controls: {
        movable: true,
        removable: true,
        position: 'bottom-left'
      },
    },
    'slider' : {},
    'counter' : {
      addLabel: 'Counter',
    },
    'date-bar' : {},
    'idea-form' : {},
    'idea-map': {},
    'idea-overview' : {},
    'idea-single' : {},
    'image' : {},
    'info-bar' : {},
    'link': {},
    'list' : {},
    'gebiedsontwikkeling-tool': {
      addLabel: 'Map for area development',
    },
    'main-image' : {},
    'one-row': { },
    'all-on-one-row': {},
    'begroot': {
      addLabel: 'Participatory budgetting',
    },
    'apostrophe-rich-text': {
      toolbar: [ 'Styles', 'Bold', 'Italic', 'Link', 'Unlink', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', ],
      styles: [
        { name: 'Paragraph', element: 'p' }
      ],
      controls: {
        movable: true,
        removable: true,
        position: 'top-left'
      }
    },

    'speech-bubble' : {
      controls: {
        position: 'top-left'
      },
    },
    'title' : {},
//      'user-form' : {},
    'local-video': {
      addLabel: 'Video (upload)',
    },
    'apostrophe-video' : {
      addLabel: 'Video (3d party, youtube, vimeo, etc.)',
    },
};
