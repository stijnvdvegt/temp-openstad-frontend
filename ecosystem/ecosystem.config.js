module.exports = {
  apps: [
    require('./app')
  ],
  deploy: {
    staging : {
      "user" : "openstadstage",
      "host" : "5.61.252.243",            
      "path" : "/home/openstadstage/domains/openstad-frontend",
      "post-deploy" : "pm2 reload ecosystem.config.js --env staging"
    },
  }
}
